package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.model.Student;

@WebServlet("/GetStudentById")
public class GetStudentById extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int StudentId = Integer.parseInt(request.getParameter("StudentId"));
		
		StudentDAO stuDao = new StudentDAO ();
		Student stu = stuDao.getStudentById(StudentId);
		
		
		if (stu != null) {
			//Storing the employee data under request object
			request.setAttribute("stu", stu);

			RequestDispatcher rd = request.getRequestDispatcher("GetStudentById.jsp");
			rd.forward(request, response);

		} else {
			RequestDispatcher rd = request.getRequestDispatcher("AdminHomePage.jsp");
			rd.include(request, response);
			
			out.println("<h3 style='color:red;' align='center'>Student Record Not Found!!!</h3>");
		}
		
		
		out.println("</center></body>");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}

