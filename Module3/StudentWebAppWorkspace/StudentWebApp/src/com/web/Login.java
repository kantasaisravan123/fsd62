package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.StudentDAO;
import com.model.Student;

@WebServlet("/LoginServlet")
public class Login extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");

		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);

		out.println("<html>");
		out.println("<body bgcolor='lightyellow'><center>");

		if (emailId.equalsIgnoreCase("Admin") && password.equals("Admin")) {
			RequestDispatcher rd = request.getRequestDispatcher("AdminHomePage.jsp");
			rd.forward(request, response);			
		} else {

			//For Employee login
			StudentDAO stuDao = new StudentDAO();
			Student stu = stuDao.stuLogin(emailId, password);
			
			if (stu != null) {
				session.setAttribute("stu", stu);
				
				RequestDispatcher rd = request.getRequestDispatcher("StudentHomePage.jsp");
				rd.forward(request, response);	

			} else {
				out.println("<h1 style='color:red;'>Invalid Credentials</h1>");			
				RequestDispatcher rd = request.getRequestDispatcher("Login.jsp");
				rd.include(request, response);		
			}			
			//For Employee login
		}

		out.println("</center></body></html>");		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}



