<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Student</title>
</head>
<body>
<jsp:include page="AdminHomePage.jsp" />
	<br/>

	<form action="UpdateStu" method="post">

	<table align="center">
	
		<tr>
			<td>StudentId: </td>
			<td><input type="text" name="studentId" value='${stu.studentId}' readonly /></td>
		</tr>
	
		<tr>
			<td>StudentName: </td>
			<td><input type="text" name="studentName" value='${stu.studentName}' /></td>
		</tr>
		
		<tr>
			<td>Degree: </td>
			<td><input type="text" name="degree" value='${stu.degree}' /></td>
		</tr>
		
		<tr>
			<td>Gender: </td>
			<td>
				<select name="gender">
					<option value="${stu.gender}" selected> ${stu.gender} </option>
					<option value="Male"  > Male </option>
					<option value="Female"> Female </option>
					<option value="Other" > Others </option>
				</select>
			</td>
		</tr>
		
		<tr>
			<td>Email-Id: </td>
			<td><input type="text" name="emailId" value='${stu.emailId}' readonly /></td>
		</tr>
		
		<tr>
			<td>Password: </td>
			<td><input type="password" name="password" value='${stu.password}' readonly /></td>
		</tr>
		
		
		<tr>
			<td></td>
			<td>
				<button>Update Student</button>
			</td>
		</tr>
	</table>	
</form>
</body>
</html>