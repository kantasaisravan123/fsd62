<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>
<h1>Login Form</h1>

<form action="LoginServlet" method="post">

	<table>
		<tr>
			<td>Enter Email-Id</td>
			<td><input type="text" name="emailId" /></td>
		</tr>
		
		<tr>
			<td>Enter Password</td>
			<td><input type="password" name="password" /></td>
		</tr>		
		
		<tr>
			<td></td>
			<td>
				<button>Login</button> 
				New User? <a href='Register.jsp'>SignUp</a>
			</td>
		</tr>
	</table>	
</form>

</body>
</html>