<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.model.Student, java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllEmployees</title>
</head>
<body>

	<jsp:include page="AdminHomePage.jsp" />

	<br />
	<table align='center' border=2>
		<tr>
			<th>studentId</th>
			<th>studentName</th>
			<th>degree</th>
			<th>gender</th>
			<th>emailId</th>
		</tr>

		<c:forEach var="stu" items="${stuList}">
			<tr>
				<td> ${ stu.studentId   } </td>
				<td> ${ stu.studentName } </td>
				<td> ${ stu.degree  } </td>
				<td> ${ stu.gender  } </td>
				<td> ${ stu.emailId } </td>
				
				<td> <a href='EditStu?studentId=${stu.studentId}'> Edit </a> </td>				
				<td> <a href='DeleteStu?studentId=${stu.studentId}'> Delete </a> </td>
				
			</tr>
		</c:forEach>
	</table>
</body>
</html>