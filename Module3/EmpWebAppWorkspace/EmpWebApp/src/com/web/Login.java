package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.EmployeeDAO;
import com.model.Employee;

@WebServlet("/LoginServlet")
public class Login extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		String emailId = request.getParameter("emailId");
		String password = request.getParameter("password");
		
		HttpSession session = request.getSession(true);
		session.setAttribute("emailId", emailId);

		out.println("<html>");
		out.println("<body bgcolor='lightyellow'><center>");

		if (emailId.equalsIgnoreCase("HR") && password.equals("HR")) {
			
			//Changing the link from HRHomePage(Servlet) to HRHomePage.jsp
			RequestDispatcher rd = request.getRequestDispatcher("HRHomePage.jsp");
			rd.forward(request, response);			
		} else {

			EmployeeDAO empDao = new EmployeeDAO();
			Employee emp = empDao.empLogin(emailId, password);
			
			if (emp != null) {
				
				//Storing employee data under the Session for Profile
				session.setAttribute("emp", emp);
				
				RequestDispatcher rd = request.getRequestDispatcher("EmpHomePage.jsp");
				rd.forward(request, response);	
			} else {
				out.println("<h1 style='color:red;'>Invalid Credentials</h1>");			
				RequestDispatcher rd = request.getRequestDispatcher("Login.jsp");
				rd.include(request, response);		
			}			
		}

		out.println("</center></body></html>");		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}



