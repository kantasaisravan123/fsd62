package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.GetConnection;
import com.model.Employee;

public class EmployeeDAO {

public Employee empLogin(String emailId, String password) {
		
		Connection con = GetConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "select * from employee where emailId = ? and password = ?";
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Employee emp = new Employee();
				
				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble("salary"));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	public int empRegister(Employee emp) {
		PreparedStatement pst = null;
		Connection con = GetConnection.getConnection();
		
		String insertQry = "insert into employee " + 
		"(empName, salary, gender, emailId, password) values (?, ?, ?, ?, ?)";
		
		try {
			pst = con.prepareStatement(insertQry);
			
			pst.setString(1, emp.getEmpName());
			pst.setDouble(2, emp.getSalary());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
			
			return pst.executeUpdate();	//returns 1 or 0
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return 0;

	}

	public Employee getEmployeeById(int empId) {
		
		Connection con = GetConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "select * from employee where empId = ?";
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setInt(1, empId);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Employee emp = new Employee();
				
				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	public List<Employee> getAllEmployees() {
	    List<Employee> employees = new ArrayList<>();
	    Connection con = null;
	    PreparedStatement pst = null;
	    ResultSet rs = null;
	    
	    try {
	        con = GetConnection.getConnection();
	        String selectQry = "SELECT * FROM employee";
	        pst = con.prepareStatement(selectQry);
	        rs = pst.executeQuery();
	        
	        while (rs.next()) {
	            Employee emp = new Employee();
	            emp.setEmpId(rs.getInt("empId"));
	            emp.setEmpName(rs.getString("empName"));
	            emp.setSalary(rs.getDouble("salary"));
	            emp.setGender(rs.getString("gender"));
	            emp.setEmailId(rs.getString("emailId"));
	            emp.setPassword(rs.getString("password"));
	            
	            employees.add(emp);
	        }
	    } catch (SQLException e) {
	        e.printStackTrace();
	    } finally {
	        try {
	            if (rs != null) rs.close();
	            if (pst != null) pst.close();
	            if (con != null) con.close();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    return employees;
	}

	public int deleteEmployee(int empId) {
		PreparedStatement pst = null;
		Connection con = GetConnection.getConnection();
			
		String deleteQry = "delete from employee where empId = ?";
			
		try {
			pst = con.prepareStatement(deleteQry);
			pst.setInt(1, empId);
				
			return pst.executeUpdate();	//returns 1 or 0
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		finally {			
				
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return 0;
	}

	public int updateEmployee(Employee emp) {
		
		PreparedStatement pst = null;
		Connection con = DbConnection.getConnection();
			
		String updateQry = "update employee set empName=?, salary=?, gender=?, emailId=?, password=? where empId=?";
			
		try {
			pst = con.prepareStatement(updateQry);
				
			pst.setString(1, emp.getEmpName());
			pst.setDouble(2, emp.getSalary());
			pst.setString(3, emp.getGender());
			pst.setString(4, emp.getEmailId());
			pst.setString(5, emp.getPassword());
			pst.setInt(6, emp.getEmpId());
				
			return pst.executeUpdate();	//returns 1 or 0
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		finally {			
				
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return 0;
	}

}

