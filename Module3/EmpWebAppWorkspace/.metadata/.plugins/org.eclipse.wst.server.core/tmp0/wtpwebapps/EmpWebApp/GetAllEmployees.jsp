<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" import="com.model.Employee, java.util.List"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllEmployees</title>
</head>
<body>

	<jsp:include page="HRHomePage.jsp" />

	<br />
	<table align='center' border=2>
		<tr>
			<th>EmpId</th>
			<th>EmpName</th>
			<th>Salary</th>
			<th>Gender</th>
			<th>EmailId</th>
			<th colspan='2'>Actions</th>
		</tr>

		<c:forEach var="emp" items="${empList}">
			<tr>
				<td> ${ emp.empId   } </td>
				<td> ${ emp.empName } </td>
				<td> ${ emp.salary  } </td>
				<td> ${ emp.gender  } </td>
				<td> ${ emp.emailId } </td>
				
				<!-- Providing the Delete Link -->
				<td> <a href='EditEmp?empId=${emp.empId}'> Edit </a> </td>
				<td> <a href='DeleteEmp?empId=${emp.empId}'> Delete </a> </td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>