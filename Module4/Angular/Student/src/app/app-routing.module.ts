import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowstudentsComponent } from './showstudents/showstudents.component';
import { ShowstubyidComponent } from './showstubyid/showstubyid.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [

  // {path:'',            component:LoginComponent},
  {path:'login',       component:LoginComponent},
  {path:'register',    component:RegisterComponent},
  {path:'showstus',    component:ShowstudentsComponent},
  {path:'showstubyid', component:ShowstubyidComponent},
  {path:'logout',      component:LogoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
