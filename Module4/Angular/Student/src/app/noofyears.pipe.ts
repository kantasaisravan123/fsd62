import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noofyears'
})
export class NoofyearsPipe implements PipeTransform {
  joinYear: any;
  currentYear: any;
  noofyears: any;

  transform(value: any): any {

    this.currentYear = new Date().getFullYear();
    this.joinYear = new Date(value).getFullYear();
    this.noofyears = this.currentYear - this.joinYear;

    return this.noofyears;
  }

}
