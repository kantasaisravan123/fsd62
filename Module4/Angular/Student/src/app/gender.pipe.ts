import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  transform(stuName: any, gender: any): any {

    if (gender == 'Male')
      return "Mr." + stuName;
    else if (gender == 'Female')
      return "Miss." + stuName;
    return stuName;
  }

}
