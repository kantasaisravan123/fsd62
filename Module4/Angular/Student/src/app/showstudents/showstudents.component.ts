import { Component } from '@angular/core';

@Component({
  selector: 'app-showstudents',
  templateUrl: './showstudents.component.html',
  styleUrl: './showstudents.component.css'
})
export class ShowstudentsComponent {

  students: any; 
 

  //Date Format: mm-DD-YYYY
  constructor() {
    this.students = [
      {stuId:101, stuName:'Sravan', fee:1212.12, gender:'Male',   course:'BE', doj:'08-13-2018'},
      {stuId:102, stuName:'Sai',  fee:2323.23, gender:'Male',   course:'ME', doj:'07-14-2017'},
      {stuId:103, stuName:'Sruthi', fee:3434.34, gender:'Female', course:'M.Tech', doj:'08-15-2016'},
      {stuId:104, stuName:'Kubra',  fee:4545.45, gender:'Female', course:'BSC', doj:'09-16-2015'},
      {stuId:105, stuName:'Samrat',  fee:5656.56, gender:'Male',   course:'B.COM', doj:'10-17-2014'}
    ];
  }

}
