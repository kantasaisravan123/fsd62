import { inject } from '@angular/core';
import { CanActivateFn } from '@angular/router';
import { EmpService } from './emp.service';

export const authGuard: CanActivateFn = (route, state) => {

  //Dependency Injection for EmpService
  let service = inject(EmpService);

  return service.getLoginStatus();
};
