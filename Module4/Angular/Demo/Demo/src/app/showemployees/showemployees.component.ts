import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showemployees',
  templateUrl: './showemployees.component.html',
  styleUrl: './showemployees.component.css'
})
export class ShowemployeesComponent implements OnInit {

  employees: any; 
  emailId: any;
editEmployee: any;

  constructor(private service: EmpService) {
    this.emailId = localStorage.getItem('emailId');
  }
  

  ngOnInit() {
    this.service.getAllEmployees().subscribe((data: any) => {
      console.log(data);
      this.employees = data;
    });
    
    
  }
  deleteEmployee(emp: any) {
    this.service.deleteEmployeeById(emp.empId).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.employees.findIndex((employee: any) => {
      return emp.empId == employee.empId;
    });

    this.employees.splice(i, 1);
}


}