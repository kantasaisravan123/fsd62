package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Student {
	@Id
	@GeneratedValue   //(strategy=GenerationType.AUTO)
	private int studId;
	
	@Column(name = "studName")
	private String studName;
	private String degree;
	private String emailID;
	private String password;
	
	public Student() {
	}

	public Student(int studId, String studName, String degree, String emailID, String password ) {
		this.studId = studId;
		this.studName = studName;
		this.degree = degree;
	}

	public int getStudId() {
		return studId;
	}
	public void setStudId(int studId) {
		this.studId = studId;
	}

	public String getstudName() {
		return studName;
	}
	public void setstudName(String studName) {
		this.studName = studName;
	}

	public String getDegree() {
		return degree;
	}
	public void setDegree(String degree) {
		this.degree = degree;
	}
	
	
	public String getEmailId() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Student [studId=" + studId + ", studName=" + studName + ", degree=" + degree + ", emailID=" + emailID +"]";
	}
}