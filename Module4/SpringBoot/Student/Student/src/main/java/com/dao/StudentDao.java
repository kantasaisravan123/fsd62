package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Student;

@Service
public class StudentDao {

	@Autowired	//Implementing Dependency Injection
	StudentRepository studentRepository;
	
	public List<Student> getAllStudents() {
		return studentRepository.findAll();
	}

	public Student getStudentById(int studId) {
		Student student = new Student(0, "Student Not Found!!!", "0.0", null, null);
		return studentRepository.findById(studId).orElse(student);
		
	}

	public List<Student> getStudentByName(String studName) {
		return studentRepository.findByName(studName);
	}
}
