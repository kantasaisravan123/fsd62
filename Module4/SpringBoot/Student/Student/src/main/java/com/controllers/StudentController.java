package com.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dao.StudentDao;
import com.model.Student;

@RestController
public class StudentController {
	@Autowired		//Implementing Dependency Injection
	StudentDao studentDao;
	
	@GetMapping("getAllStudents")
	public List<Student> getAllStudents() {		
		return studentDao.getAllStudents();	
	}
	


	@GetMapping("getStudentById/{id}")
	public Student getStudentById(@PathVariable("id") int studId) {
		return studentDao.getStudentById(studId);
	}
	@GetMapping("getStudentByName/{sname}")
	public List<Student> getStudentByName(@PathVariable("sname") String studName) {
		return studentDao.getStudentByName(studName);
	}
}
