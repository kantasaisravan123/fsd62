package com.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Base64;
import java.nio.charset.StandardCharsets;
import org.mindrot.jbcrypt.BCrypt;


@Entity
public class Employee {

	@Id@GeneratedValue
	private int empId;
	private String empName;
	private double salary;
	private String gender;
	private String country;
	private Date doj;
	
	@Column(unique = true)
	private String emailId;
	
	
	private String password;
	
	@ManyToOne
	@JoinColumn(name="deptId")
	Department department;
	
	public Employee() {
	}

	public Employee(int empId, String empName, double salary, String gender, String country, Date doj, String emailId,
			String password) {
		this.empId = empId;
		this.empName = empName;
		this.salary = salary;
		this.gender = gender;
		this.country = country;
		this.doj = doj;
		this.emailId = emailId;
		this.password = password;
	}
	
	

	public Department getDepartment() {
		return department;
	}
	public void setDepartment(Department department) {
		this.department = department;
	}

	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public Date getDoj() {
		return doj;
	}
	public void setDoj(Date doj) {
		this.doj = doj;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
	    this.password = Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8));
	    this.password = BCrypt.hashpw(password, BCrypt.gensalt());
	}
	
	public boolean checkPassword(String passwordToCheck) {
	    return BCrypt.checkpw(passwordToCheck, this.password);
	}


	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", salary=" + salary + ", gender=" + gender
				+ ", country=" + country + ", doj=" + doj + ", emailId=" + emailId + ", password=" + password
				+ ", department=" + department + "]";
	}

	
}