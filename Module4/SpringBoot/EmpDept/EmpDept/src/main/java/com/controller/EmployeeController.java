package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.EmployeeDao;
import com.model.Employee;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class EmployeeController {
	
	@Autowired
	EmployeeDao empDao;
	
	@GetMapping("empLogin/{emailId}/{password}")
	public Employee empLogin(@PathVariable("emailId") String emailId, 
			@PathVariable("password") String password) {
		
		return empDao.empLogin(emailId, password);
	}
	
	@GetMapping("getAllEmployees")
	public List<Employee> getAllEmployees() {
		return empDao.getAllEmployees();
	}
	
	@GetMapping("getEmployeeById/{id}")
	public Employee getEmployeeById(@PathVariable("id") int empId) {
		return empDao.getEmployeeById(empId);
	}
	
	@GetMapping("getEmployeeByName/{name}")
	public List<Employee> getEmployeeByName(@PathVariable("name") String empName) {
		return empDao.getEmployeeByName(empName);
	}
	
//	@PostMapping("addEmployee")
//	public Employee addEmployee(@RequestBody Employee emp) {
//		return empDao.addEmployee(emp);
//	}
	
	@PostMapping("addEmployee")
    public ResponseEntity<?> addEmployee(@RequestBody Employee emp) {
        try {
            return ResponseEntity.ok(empDao.addEmployee(emp));
        } catch (DataIntegrityViolationException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Email ID already exists");
        }
    }
	
	@PutMapping("updateEmployee")
	public Employee updateEmployee(@RequestBody Employee emp) {
		return empDao.updateEmployee(emp);
	}
	
	@DeleteMapping("deleteEmployeeById/{id}")
	public String deleteEmployeeById(@PathVariable("id") int empId) {
		empDao.deleteEmployeeById(empId);
		return "Employee Record Deleted Successfully!!!";
	}
}