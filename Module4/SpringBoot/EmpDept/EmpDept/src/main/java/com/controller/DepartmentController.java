package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.DepartmentDao;
import com.model.Department;

@RestController
@CrossOrigin(origins="http://localhost:4200")
public class DepartmentController {
	
	@Autowired
	DepartmentDao deptDao;
	
	@GetMapping("getAllDepartments")
	public List<Department> getAllDepartments() {
		return deptDao.getAllDepartments();
	}
	
	@GetMapping("getDepartmentById/{id}")
	public Department getDepartmentById(@PathVariable("id") int deptId) {
		return deptDao.getDepartmentById(deptId);
	}
		
	@PostMapping("addDepartment")
	public Department addDepartment(@RequestBody Department dept) {
		return deptDao.addDepartment(dept);
	}
	
	@PutMapping("updateDepartment")
	public Department updateDepartment(@RequestBody Department dept) {
		return deptDao.updateDepartment(dept);
	}
	
	@DeleteMapping("deleteDepartmentById/{id}")
	public String deleteDepartmentById(@PathVariable("id") int deptId) {
		deptDao.deleteDepartmentById(deptId);
		return "Department Deleted Successfully!!";
	}
}