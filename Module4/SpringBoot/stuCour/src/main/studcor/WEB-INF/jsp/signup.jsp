<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Sign Up</title>
</head>
<body>
    <h2>Sign Up</h2>
    <form action="signup" method="post">
        <label for="stuName">Name:</label><br>
        <input type="text" id="stuName" name="stuName" required><br>
        <label for="emailId">Email:</label><br>
        <input type="email" id="emailId" name="emailId" required><br>
        <label for="password">Password:</label><br>
        <input type="password" id="password" name="password" required><br>
        <label for="phoneNumber">Phone Number:</label><br>
        <input type="tel" id="phoneNumber" name="phoneNumber" required><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
