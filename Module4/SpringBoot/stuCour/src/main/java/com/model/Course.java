package com.model;



import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Course {
	
	@Id
	private int courId;
	private String courName;
	private String location;
	
	@OneToMany(mappedBy="course")
	@JsonIgnore    //Restricting the user to fetch the student details when the user is fetching course
	List<Student> stuList = new ArrayList<Student>();
	
	public Course() {
	}

	public Course(int courId, String courName, String location) {
		this.courId = courId;
		this.courName = courName;
		this.location = location;
	}
	
	
	public List<Student> getStuList() {
		return stuList;
	}
	public void setStuList(List<Student> stuList) {
		this.stuList = stuList;
	}

	public int getCourId() {
		return courId;
	}
	public void setCourId(int courId) {
		this.courId = courId;
	}

	public String getCourName() {
		return courName;
	}
	public void setCourName(String courName) {
		this.courName = courName;
	}

	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "Department [courId=" + courId + ", courName=" + courName + ", location=" + location + ", stuList="
				+ stuList + "]";
	}

	
}
