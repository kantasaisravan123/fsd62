package com.model;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.mindrot.jbcrypt.BCrypt;

@Entity
public class  Student{

	@Id@GeneratedValue
	private int stuId;
	private String stuName;
	private String emailId;
	private String password;
	private long phoneNumber;
	@ManyToOne
	@JoinColumn(name="courId")
	Course course;
	
	public Student() {
	}

	public Student(int stuId, String stuName,  String emailId,String password,long phoneNumber) {
		this.stuId = stuId;
		this.stuName = stuName;
		this.emailId = emailId;
		this.password = password;
		this.phoneNumber = phoneNumber;
	}
	
	

	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}

	public int getStuId() {
		return stuId;
	}
	public void setStuId(int stuId) {
		this.stuId = stuId;
	}

	public String getstuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
	    this.password = Base64.getEncoder().encodeToString(password.getBytes(StandardCharsets.UTF_8));
	    this.password = BCrypt.hashpw(password, BCrypt.gensalt());
	}
	
	public boolean checkPassword(String passwordToCheck) {
	    return BCrypt.checkpw(passwordToCheck, this.password);
	}


	public long getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	

	@Override
	public String toString() {
		return "Employee [stuId=" + stuId + ", stuName=" + stuName + ", emailId=" + emailId + ", password=" + password + ",phoneNumber=" + phoneNumber
				+ ", course=" + course + "]";
	}
}
