package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Course;

@Service
public class CourseDao {

	@Autowired
	CourseRepository courRepo;

	public List<Course> getAllCourses() {
		return courRepo.findAll();
	}
	
	public Course getCourseById(int courId) {
		return courRepo.findById(courId).orElse(null);
	}

	public Course addCourse(Course cour) {
		return courRepo.save(cour);
	}

	public Course updateCourse(Course cour) {
		return courRepo.save(cour);
	}

	public void deleteCourseById(int courId) {
		courRepo.deleteById(courId);
	}
	
}

