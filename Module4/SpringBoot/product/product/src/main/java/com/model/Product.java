package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity	//Making the Product class as an Entity Class which will be created as a table in the database.
public class Product {
	
	//@Id makes the column as an primary key column
	//@GeneratedValue make the prodId value to be generated automatically (auto_increment), 
	//@GeneratedValue(strategy=GenerationType.AUTO)
	//@Column will provide the column name to be provided when the table is created
	
	@Id@GeneratedValue		
	private int prodId;
	
	@Column(name="prodName")
	private String prodName;
	private String price;
	
	public Product() {
	}

	public Product(int prodId, String prodName, String price) {
		this.prodId = prodId;
		this.prodName = prodName;
		this.price = price;
	}

	public int getProdId() {
		return prodId;
	}
	public void setProdId(int prodId) {
		this.prodId = prodId;
	}

	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Product [prodId=" + prodId + ", prodName=" + prodName + ", price=" + price + "]";
	}
}